extern crate image;

extern crate raytracer;

#[cfg(test)]
mod tests {
    use image;
    use image::{
        DynamicImage,
        GenericImage,
        PNG,
    };
    use raytracer::rendering::{
        DirectionalLight,
        Light,
        Scene,
        SphericalLight,
    };
    use raytracer::element::{
        Element,
        Plane,
        Sphere,
    };

    use raytracer::render;
    use raytracer::point::Point;
    use raytracer::color::Color;
    use raytracer::material::{
        Material,
        Coloration,
        Surface,
    };
    use raytracer::vector::Vector3;

    use std::fs::File;
    use std::path::Path;

    #[test]
    fn test_can_render_scene() {
        let sphere1 = Sphere {
            center: Point { x: 0.0, y: 0.0, z: -2.0, },
            radius: 1.0,
            material: Material {
                coloration: Coloration::Color(Color { red: 0.4, green: 1.0, blue: 0.4 }),
                albedo: 0.8,
                surface: Surface::Refractive { index: 1.0, transparency: 0.5, },
            },
        };
        let texture_wall = image::open("assets/wall.jpg").unwrap();
        let sphere2 = Sphere {
            center: Point { x: -0.5, y: -0.5, z: -1.08, },
            radius: 0.1,
            material: Material {
                coloration: Coloration::Texture(texture_wall),
                albedo: 0.1,
                surface: Surface::Diffuse,
            },
        };
        let texture_chess = image::open("assets/chessboard.png").unwrap();
        let floor = Plane {
            pt: Point { x: 0.0, y: -0.50, z: 0.0, },
            normal: Vector3 { x: 0.0, y: -1.0, z: 0.10, },
            material: Material {
                coloration: Coloration::Texture(texture_chess),
                albedo: 0.1,
                surface: Surface::Reflective{ reflectivity: 0.5 },
            },
        };
        let wall_front = Plane {
            pt: Point { x: 0.0, y: 0.0, z: -10.0, },
            normal: Vector3 { x: 0.0, y: 0.0, z: -1.0, },
            material: Material {
                coloration: Coloration::Color(Color { red: 0.0, green: 0.3, blue: 1.0 }),
                albedo: 0.1,
                surface: Surface::Diffuse,
            },
        };
        let wall_back = Plane {
            pt: Point { x: 0.0, y: 0.0, z: 10.0, },
            normal: Vector3 { x: 0.0, y: 0.0, z: 1.0, },
            material: Material {
                coloration: Coloration::Color(Color { red: 1.0, green: 0.3, blue: 0.0 }),
                albedo: 0.1,
                surface: Surface::Diffuse,
            },
        };
        let vec_elts = vec![
            Element::Sphere(sphere1),
            Element::Sphere(sphere2),
            Element::Plane(floor),
            Element::Plane(wall_front),
            Element::Plane(wall_back),
        ];

        let directional_light = DirectionalLight {
            color: Color { red: 1.0, green: 1.0, blue: 1.0, },
            direction: Vector3 { x: -1.0, y: -1.0, z: 0.0, },
            intensity: 0.7,
        };

        let spherical_light = SphericalLight {
            color: Color { red: 1.0, green: 1.0, blue: 1.0, },
            position: Point { x: 1.0, y: 1.0, z: 1.0, },
            intensity: 1000.0,
        };

        let vec_lights = vec![
            Light::Directional(directional_light),
            Light::Spherical(spherical_light),
        ];
        let scene = Scene {
            elements: vec_elts,
            fov: 90.0,
            height: 600,
            lights: vec_lights,
            width: 800,
            shadow_bias: 1e-13,
            max_recursion: 3,
        };

        let img: DynamicImage = render(&scene);
        assert_eq!(scene.width, img.width());
        assert_eq!(scene.height, img.height());

        let ref mut fout = File::create(&Path::new("scene_1.png")).unwrap();
        let _ = img.save(fout, PNG);
    }
}
