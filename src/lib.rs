extern crate image;

pub mod color;
pub mod element;
pub mod intersect;
pub mod material;
pub mod point;
pub mod rendering;
pub mod vector;

use image::{
    DynamicImage,
    GenericImage,
};
use rendering::{
    Ray,
    Scene,
};

pub fn render(scene: &Scene) -> DynamicImage {
    let mut img = DynamicImage::new_rgb8(scene.width, scene.height);
    for x in 0..scene.width {
        for y in 0..scene.height {
            let ray = Ray::create_prime(x, y, scene);
            img.put_pixel(x, y, scene.cast_ray(&ray, 0).to_rgba());
        }
    }
    img
}
