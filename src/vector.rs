use std::ops::{
    Add,
    Sub,
    Mul,
    Neg,
};
use point::Point;

#[derive(Clone, Copy)]
pub struct Vector3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vector3 {
    pub fn fill(v: f64) -> Vector3 {
        Vector3 {
            x: v,
            y: v,
            z: v,
        }
    }

    pub fn zero() -> Vector3 {
        Vector3::fill(0.0)
    }

    pub fn norm(&self) -> f64 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn length(&self) -> f64 {
        self.norm().sqrt()
    }

    pub fn normalize(&self) -> Vector3 {
        let inv_len = self.length().recip();
        Vector3 {
            x: self.x * inv_len,
            y: self.y * inv_len,
            z: self.z * inv_len,
        }
    }

    pub fn dot(&self, vect: &Vector3) -> f64 {
        self.x * vect.x + self.y * vect.y + self.z * vect.z
    }

    pub fn cross(&self, vect: &Vector3) -> Vector3 {
        Vector3 {
            x: self.y * vect.z - self.z * vect.y,
            y: self.z * vect.x - self.x * vect.z,
            z: self.x * vect.y - self.y * vect.x,
        }
    }

    pub fn create_from_pt(pt1: &Point, pt2: &Point) -> Vector3 {
        Vector3 {
            x: pt1.x - pt2.x,
            y: pt1.y - pt2.y,
            z: pt1.z - pt2.z,
        }
    }
}

impl Neg for Vector3 {
    type Output = Vector3;
    fn neg(self) -> Vector3 {
        Vector3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl<'a> Neg for &'a Vector3 {
    type Output = Vector3;
    fn neg(self) -> Vector3 {
        Vector3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Add for Vector3 {
    type Output = Vector3;
    fn add(self, vect: Vector3) -> Vector3 {
        Vector3 {
            x: self.x + vect.x,
            y: self.y + vect.y,
            z: self.z + vect.z,
        }
    }
}

impl<'a> Add<Vector3> for &'a Vector3 {
    type Output = Vector3;
    fn add(self, vect: Vector3) -> Vector3 {
        Vector3 {
            x: self.x + vect.x,
            y: self.y + vect.y,
            z: self.z + vect.z,
        }
    }
}

impl Sub for Vector3 {
    type Output = Vector3;
    fn sub(self, vect: Vector3) -> Vector3 {
        Vector3 {
            x: self.x - vect.x,
            y: self.y - vect.y,
            z: self.z - vect.z,
        }
    }
}

impl<'a> Sub<&'a Vector3> for Vector3 {
    type Output = Vector3;
    fn sub(self, vect: &Vector3) -> Vector3 {
        Vector3 {
            x: self.x - vect.x,
            y: self.y - vect.y,
            z: self.z - vect.z,
        }
    }
}

impl<'a> Sub<Vector3> for &'a Vector3 {
    type Output = Vector3;
    fn sub(self, vect: Vector3) -> Vector3 {
        Vector3 {
            x: self.x - vect.x,
            y: self.y - vect.y,
            z: self.z - vect.z,
        }
    }
}

impl<'a, 'b> Sub<&'a Vector3> for &'b Vector3 {
    type Output = Vector3;
    fn sub(self, vect: &Vector3) -> Vector3 {
        Vector3 {
            x: self.x - vect.x,
            y: self.y - vect.y,
            z: self.z - vect.z,
        }
    }
}

impl Mul for Vector3 {
    type Output = Vector3;
    fn mul(self, vect: Vector3) -> Vector3 {
        Vector3 {
            x: self.x * vect.x,
            y: self.y * vect.y,
            z: self.z * vect.z,
        }
    }
}

impl<'a, 'b> Mul<&'a Vector3> for &'b Vector3 {
    type Output = Vector3;
    fn mul(self, vect: &Vector3) -> Vector3 {
        Vector3 {
            x: self.x * vect.x,
            y: self.y * vect.y,
            z: self.z * vect.z,
        }
    }
}

impl Mul<f64> for Vector3 {
    type Output = Vector3;
    fn mul(self, f: f64) -> Vector3 {
        Vector3 {
            x: self.x * f,
            y: self.y * f,
            z: self.z * f,
        }
    }
}

impl Mul<Vector3> for f64{
    type Output = Vector3;
    fn mul(self, vect: Vector3) -> Vector3 {
        vect * self
    }
}

impl<'a> Mul<f64> for &'a Vector3 {
    type Output = Vector3;
    fn mul(self, f: f64) -> Vector3 {
        Vector3 {
            x: self.x * f,
            y: self.y * f,
            z: self.z * f,
        }
    }
}

impl<'a> Mul<&'a Vector3> for f64{
    type Output = Vector3;
    fn mul(self, vect: &Vector3) -> Vector3 {
        vect * self
    }
}
