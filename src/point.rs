use std::ops::{
    Add,
    Sub,
    Neg,
};

use vector::Vector3;

#[derive(Clone)]
pub struct Point {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Point {
    pub fn new(vec: Vector3) -> Point {
        Point { x: vec.x, y: vec.y, z: vec.z, }
    }

    pub fn fill(v: f64) -> Point {
        Point { x: v, y: v, z: v, }
    }

    pub fn zero() -> Point {
        Point::fill(0.0)
    }
}

impl Neg for Point {
    type Output = Point;
    fn neg(self) -> Point {
        Point {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Add<Vector3> for Point {
    type Output = Point;
    fn add(self, vect: Vector3) -> Point {
        Point {
            x: self.x + vect.x,
            y: self.y + vect.y,
            z: self.z + vect.z,
        }
    }
}

impl<'a> Add<Vector3> for &'a Point {
    type Output = Point;
    fn add(self, vect: Vector3) -> Point {
        Point {
            x: self.x + vect.x,
            y: self.y + vect.y,
            z: self.z + vect.z,
        }
    }
}

impl Add<Point> for Vector3 {
    type Output = Point;
    fn add(self, pt: Point) -> Point {
        pt + self
    }
}

impl Sub<Vector3> for Point {
    type Output = Point;
    fn sub(self, other: Vector3) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Sub<Point> for Vector3 {
    type Output = Point;
    fn sub(self, pt: Point) -> Point {
        pt - self
    }
}

impl<'a, 'b> Sub<&'b Point> for &'a Point {
    type Output = Vector3;

    fn sub(self, pt: &Point) -> Vector3 {
        Vector3 {
            x: self.x - pt.x,
            y: self.y - pt.y,
            z: self.z - pt.z,
        }
    }
}

impl Sub<Point> for Point {
    type Output = Vector3;

    fn sub(self, pt: Point) -> Vector3 {
        Vector3 {
            x: self.x - pt.x,
            y: self.y - pt.y,
            z: self.z - pt.z,
        }
    }
}
