use color::Color;
use element::*;
use intersect::*;
use point::Point;
use vector::Vector3;
use material::*;

use std::f64;
use std::f32::consts::PI;

/*-- Light -------------------------------------------------------------------*/

pub struct DirectionalLight {
    pub direction: Vector3,
    pub color: Color,
    pub intensity: f32,
}

pub struct SphericalLight {
    pub position: Point,
    pub color: Color,
    pub intensity: f32,
}

pub enum Light {
    Directional(DirectionalLight),
    Spherical(SphericalLight)
}

impl Light {
    pub fn color(&self) -> &Color {
        match *self {
            Light::Directional(ref d) => &d.color,
            Light::Spherical(ref s) => &s.color,
        }
    }

    pub fn direction_from(&self, hit_point: &Point) -> Vector3 {
        match *self {
            Light::Directional(ref d) => -d.direction,
            Light::Spherical(ref s) => (&s.position - hit_point).normalize(),
        }
    }

    pub fn intensity(&self, hit_point: &Point) -> f32 {
        match *self {
            Light::Directional(ref d) => d.intensity,
            Light::Spherical(ref s) => {
                let n = (&s.position - hit_point).norm() as f32;
                s.intensity / (4.0 * PI * n)
            },
        }
    }

    pub fn distance(&self, hit_point: &Point) -> f64 {
        match *self {
            Light::Directional(_) => f64::INFINITY,
            Light::Spherical(ref s) => (&s.position - hit_point).length(),
        }
    }
}

/*-- Ray --------------------------------------------------------------------*/

pub struct Ray {
    pub direction: Vector3,
    pub origin: Point,
}

impl Ray {
    pub fn create_prime(x: u32, y: u32, scene: &Scene) -> Ray {
        assert!(scene.width > scene.height);

        let aspect_ratio = (scene.width as f64) / (scene.height as f64);
        let fov_adjustement = (scene.fov.to_radians() / 2.0).tan();

        let sensor_x = (((x as f64 + 0.5) / scene.width as f64) * 2.0 - 1.0)
                     * aspect_ratio;
        let sensor_y = (1.0 - ((y as f64 + 0.5) / scene.height as f64) * 2.0)
                     * fov_adjustement;
        let direction = Vector3 {
            // Take center, normalize it and adjust to screen pos.
            x: sensor_x,
            y: sensor_y,
            z: -1.0,
        };

        Ray {
            origin: Point::zero(),
            direction: direction.normalize(),
        }
    }

    pub fn create_reflection(normal: &Vector3,
                             incident: &Vector3,
                             hit_point: &Point,
                             bias: f64)
                                -> Ray {
        Ray {
            origin: hit_point + (normal * bias),
            direction: incident - (2.0 * incident.dot(&normal) * normal),
        }
    }

    pub fn create_refraction(normal: &Vector3,
                             incident: &Vector3,
                             hit_point: &Point,
                             bias: f64,
                             index: f32)
                                -> Option<Ray> {
        let mut i_normal = incident.dot(normal);
        let r_normal;
        let eta_t;
        let eta_i;
        if i_normal < 0.0 {
            i_normal = -i_normal;
            r_normal = normal.clone();
            eta_t = index as f64;
            eta_i = 1.0 as f64;
        } else {
            r_normal = -normal;
            eta_t = 1.0 as f64;
            eta_i = index as f64;
        }

        let eta = eta_i / eta_t;
        let k = 1.0 - (eta * eta) * (1.0 - i_normal * i_normal);
        if k < 0.0 {
            return None;
        }

        Some(Ray {
            origin: Point::new(incident + (r_normal * -bias)),
            direction: (incident + i_normal * r_normal) * eta - r_normal * k.sqrt(),
        })
    }

    pub fn fresnel(normal: &Vector3, incident: &Vector3, index: f32) -> f64 {
        let i_normal = incident.dot(normal);
        let eta_i;
        let eta_t;
        if i_normal <= 0.0 {
            eta_i = 1.0;
            eta_t = index as f64;
        } else {
            eta_i = index as f64;
            eta_t = 1.0;
        }

        let sin_t = eta_i / eta_t * (1.0 - i_normal * i_normal).max(0.0).sqrt();
        if sin_t < 1.0 {
            1.0
        } else {
            let cos_t = (1.0 - sin_t * sin_t).max(0.0).sqrt();
            let cos_i = cos_t.abs();
            let r_s = ((eta_t * cos_i) - (eta_i * cos_t)) / ((eta_t * cos_i) + (eta_i * cos_t));
            let r_p = ((eta_i * cos_i) - (eta_t * cos_t)) / ((eta_i * cos_i) + (eta_t * cos_t));
            (r_s * r_s + r_p * r_p) / 2.0
        }
    }
}


/*-- Scene -----------------------------------------------------------------*/

pub struct Scene {
    pub elements: Vec<Element>,
    pub fov: f64,
    pub height: u32,
    pub lights: Vec<Light>,
    pub width: u32,
    pub shadow_bias: f64,
    pub max_recursion: u32,
}

impl Scene {
    pub fn cast_ray(&self, ray: &Ray, depth: u32) -> Color {
        let black = Color::fill(0.0);
        if depth >= self.max_recursion {
            return black;
        }

        let intersection = self.trace(&ray);
        intersection.map(|i| self.get_color(ray, &i, depth)).unwrap_or(black)
    }

    pub fn trace(&self, ray: &Ray) -> Option<Intersection> {
        let mut intersection: Option<Intersection> = None;
        let mut min = f64::INFINITY;

        for e in self.elements.as_slice() {
            let d = e.intersect(ray);
            match d {
                None => continue,
                Some(x) => if min.is_infinite() || x < min {
                    min = x;
                    intersection = Some(Intersection::new(x, &e));
                }
            }
        }
        intersection
    }

    fn get_diffuse(&self, hit_point: &Point, normal: &Vector3, element: &Element) -> Color {
        let mut color = Color::fill(0.0);

        for light in &self.lights {
            let direction_to_light = light.direction_from(hit_point);

            let shadow_ray = Ray {
                origin: hit_point + (normal * self.shadow_bias),
                direction: direction_to_light,
            };
            let shadow_intersection = self.trace(&shadow_ray);
            let in_light = shadow_intersection.is_none()
                || shadow_intersection.unwrap().dist > light.distance(hit_point);

            let light_intensity =
                if in_light { light.intensity(hit_point) }
                else { 0.0 };
            let light_power = (normal.dot(&direction_to_light) as f32).max(0.0)
                * light_intensity;
            let light_reflected = element.albedo() / PI;
            let light_color = element.color(hit_point)
                * light.color()
                * light_power
                * light_reflected;
            color = color + light_color;
        }
        color.clamp()
    }

    fn get_color(&self, ray: &Ray, intersection: &Intersection, depth: u32) -> Color {
        let hit_point = ray.origin.clone() + (ray.direction * intersection.dist);
        let normal = intersection.elt.normal(&hit_point);
        let material = intersection.elt.material();

        match material.surface {
            Surface::Diffuse =>
                self.get_diffuse(&hit_point, &normal, &intersection.elt),

            Surface::Reflective{ reflectivity } => {
                let mut color = self.get_diffuse(&hit_point, &normal, &intersection.elt);
                let reflection_ray =
                    Ray::create_reflection(&normal, &ray.direction, &hit_point, self.shadow_bias);
                color = color * (1.0 - reflectivity);
                color + (reflectivity * self.cast_ray(&reflection_ray, depth + 1))
            },

            Surface::Refractive { index, transparency } => {
                let refraction_color;
                let kr = Ray::fresnel(&ray.direction, &normal, index) as f32;
                if kr < 1.0 {
                    let refraction_ray =
                        Ray::create_refraction(&normal, &ray.direction, &hit_point, self.shadow_bias, index);
                    refraction_color = self.cast_ray(&refraction_ray.unwrap(), depth + 1);
                } else {
                    refraction_color = Color::fill(0.0);
                }

                let surface_color = material.coloration
                    .color(&intersection.elt.texture_coords(&hit_point));
                let reflection_ray =
                    Ray::create_reflection(&normal, &ray.direction, &hit_point, self.shadow_bias);
                let reflection_color = self.cast_ray(&reflection_ray, depth + 1);
                (reflection_color * kr + refraction_color * (1.0 - kr))
                    * transparency * surface_color
            },
        }
    }
}
