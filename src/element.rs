use std::f32::consts::PI;

use color::Color;
use intersect::*;
use material::*;
use point::Point;
use rendering::Ray;
use vector::Vector3;

/*-- Element ----------------------------------------------------------------*/

pub enum Element {
    Plane(Plane),
    Sphere(Sphere),
}

impl Element {
    pub fn color(&self, hit_point: &Point) -> Color {
        match *self {
            Element::Plane(ref p) => p.material.coloration.color(&p.texture_coords(hit_point)),
            Element::Sphere(ref s) => s.material.coloration.color(&s.texture_coords(hit_point)),
        }
    }

    pub fn albedo(&self) -> f32{
        match *self {
            Element::Plane(ref p) => p.material.albedo,
            Element::Sphere(ref s) => s.material.albedo,
        }
    }

    pub fn material(&self) -> &Material {
        match *self {
            Element::Plane(ref p) => &p.material,
            Element::Sphere(ref s) => &s.material,
        }
    }
}

impl Intersectable for Element {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        match *self {
            Element::Plane(ref p) => p.intersect(ray),
            Element::Sphere(ref s) => s.intersect(ray),
        }
    }

    fn normal(&self, hit_point: &Point) -> Vector3 {
        match *self {
            Element::Plane(ref p) => p.normal(hit_point),
            Element::Sphere(ref s) => s.normal(hit_point),
        }
    }

    fn texture_coords(&self, hit_point: &Point) -> TextureCoords {
        match *self {
            Element::Plane(ref p) => p.texture_coords(hit_point),
            Element::Sphere(ref s) => s.texture_coords(hit_point),
        }
    }
}

/*-- Plane ------------------------------------------------------------------*/

pub struct Plane {
    pub pt: Point,
    pub normal: Vector3,
    pub material: Material,
}

impl Intersectable for Plane {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        let normal = &self.normal;
        let denom = normal.dot(&ray.direction);
        if denom < 1e-6 {
            return None;
        }

        let v = Vector3::create_from_pt(&self.pt, &ray.origin);
        let distance = v.dot(&normal) / denom;
        if distance < 0.0 {
            return None;
        }

        Some(distance)
    }

    fn normal(&self, hit_point: &Point) -> Vector3 {
        -self.normal.clone()
    }

    fn texture_coords(&self, hit_point: &Point) -> TextureCoords {

        let z = Vector3 { x :0.0, y: 0.0, z: 1.0, };
        let mut x_axis = self.normal.cross(&z);
        if x_axis.length() == 0.0 {
            let y = Vector3 { x :0.0, y: 1.0, z: 0.0, };
            x_axis = self.normal.cross(&y);
        };
        let y_axis = self.normal.cross(&x_axis);

        let hit_vec = hit_point - &self.pt;
        TextureCoords {
            x: hit_vec.dot(&x_axis) as f32,
            y: hit_vec.dot(&y_axis) as f32,
        }
    }
}

/*-- Sphere -----------------------------------------------------------------*/

pub struct Sphere {
    pub center: Point,
    pub radius: f64,
    pub material: Material,
}

impl Intersectable for Sphere {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        // Segment for origin to center.
        let line: Vector3 = Vector3::create_from_pt(&self.center, &ray.origin);
        // Adjacent side of line (with line as hypothenuse).
        let adj = line.dot(&ray.direction);
        // Get square of dist between self.center and ajd.
        let sq_dist = line.dot(&line) - (adj * adj);

        // If sq_dist < sq_radius, it does not intersect.
        let sq_radius = self.radius * self.radius;
        if sq_dist > sq_radius {
            return None;
        }

        let thickness = (sq_radius - sq_dist).sqrt();
        let bot_dist = adj - thickness;
        let top_dist = adj + thickness;
        if top_dist < 0.0 && bot_dist < 0.0 {
            None
        } else if bot_dist < 0.0 {
            Some(top_dist)
        } else if top_dist < 0.0 {
            Some(bot_dist)
        } else if bot_dist < top_dist {
            Some(bot_dist)
        } else {
            Some(top_dist)
        }
    }

    fn normal(&self, hit_point: &Point) -> Vector3 {
        Vector3::create_from_pt(hit_point, &self.center).normalize()
    }

    fn texture_coords(&self, hit_point: &Point) -> TextureCoords {
        let hit_vec = hit_point - &self.center;
        TextureCoords {
            x: (1.0 + (hit_vec.z.atan2(hit_vec.x) as f32) / PI) * 0.5,
            y: (hit_vec.y / self.radius).acos() as f32 / PI,
        }
    }
}
