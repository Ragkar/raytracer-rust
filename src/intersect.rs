use element::Element;
use point::Point;
use rendering::Ray;
use vector::Vector3;
use material::TextureCoords;

/*-- Intersectable ----------------------------------------------------------*/

pub trait Intersectable {
    fn intersect(&self, ray: &Ray) -> Option<f64>;
    fn normal(&self, hit_point: &Point) -> Vector3;
    fn texture_coords(&self, hit_point: &Point) -> TextureCoords;
}

/*-- Intersection -----------------------------------------------------------*/

pub struct Intersection<'a> {
    pub dist: f64,
    pub elt: &'a Element,
}

impl <'a> Intersection<'a> {
    pub fn new<'b>(distance: f64, element: &'b Element) -> Intersection<'b> {
        Intersection {
            dist: distance,
            elt: element,
        }
    }
}

