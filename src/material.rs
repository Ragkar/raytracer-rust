use image::{
    DynamicImage,
    GenericImage,
};

use color::Color;

pub struct TextureCoords {
    pub x: f32,
    pub y: f32,
}

pub enum Coloration {
    Color(Color),
    Texture(DynamicImage),
}

pub enum Surface {
    Diffuse,
    Reflective { reflectivity: f32 },
    Refractive { index: f32, transparency: f32 },
}

pub struct Material {
    pub coloration: Coloration,
    pub albedo: f32,
    pub surface: Surface,
}

fn wrap(val: f32, bound: u32) -> u32 {
    let signed_bound = bound as i32;
    let float_coord = val * bound as f32;
    let wrapped_coord = (float_coord as i32) % signed_bound;

    if wrapped_coord < 0 {
        (wrapped_coord + signed_bound) as u32
    } else {
        wrapped_coord as u32
    }
}

impl Coloration {
    pub fn color(&self, texture_coords: &TextureCoords) -> Color {
        match *self {
            Coloration::Color(ref c) => c.clone(),
            Coloration::Texture(ref t) => {
                let x = wrap(texture_coords.x, t.width());
                let y = wrap(texture_coords.y, t.height());
                Color::from_rgba(t.get_pixel(x, y))
            }
        }
    }
}
